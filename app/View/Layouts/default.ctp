<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
	<?php
		echo $this->Html->css(['bootstrap.min', 'light-bootstrap-dashboard']);
		echo $this->Html->script(['core/jquery.3.2.1.min', 'core/popper.min', 'core/bootstrap.min', 'light-bootstrap-dashboard']);
	?>
</head>
<body>
	<div class="wrapper">
		<div class="sidebar" data-image="/img/sidebar-5.jpg">
			<div class="sidebar-wrapper">
				<div class="logo">
					<a href="http://www.creative-tim.com" class="simple-text">
						MicroE
					</a>
				</div>
				<ul class="nav">
					<li class="nav-item active">
						<a class="nav-link" href="dashboard.html">
							<i class="nc-icon nc-chart-pie-35"></i>
							<p>Dashboard</p>
						</a>
					</li>
					<li>
						<a class="nav-link" href="./user.html">
							<i class="nc-icon nc-circle-09"></i>
							<p>User Profile</p>
						</a>
					</li>
					<li>
						<a class="nav-link" href="./table.html">
							<i class="nc-icon nc-notes"></i>
							<p>Table List</p>
						</a>
					</li>
					<li>
						<a class="nav-link" href="./typography.html">
							<i class="nc-icon nc-paper-2"></i>
							<p>Typography</p>
						</a>
					</li>
					<li>
						<a class="nav-link" href="./icons.html">
							<i class="nc-icon nc-atom"></i>
							<p>Icons</p>
						</a>
					</li>
					<li>
						<a class="nav-link" href="./maps.html">
							<i class="nc-icon nc-pin-3"></i>
							<p>Maps</p>
						</a>
					</li>
					<li>
						<a class="nav-link" href="./notifications.html">
							<i class="nc-icon nc-bell-55"></i>
							<p>Notifications</p>
						</a>
					</li>
				</ul>
			</div>
		</div>
		
		<div class="main-panel">
			<!-- Navbar -->
            <nav class="navbar navbar-expand-lg " color-on-scroll="500">
                <div class=" container-fluid  ">
                    <a class="navbar-brand" href="#pablo"> Dashboard </a>
                    <button href="" class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <ul class="nav navbar-nav mr-auto">
                            <li class="nav-item">
                                <a href="#" class="nav-link" data-toggle="dropdown">
                                    <i class="nc-icon nc-palette"></i>
                                    <span class="d-lg-none">Dashboard</span>
                                </a>
                            </li>
                            <li class="dropdown nav-item">
                                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                    <i class="nc-icon nc-planet"></i>
                                    <span class="notification">5</span>
                                    <span class="d-lg-none">Notification</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Notification 1</a>
                                    <a class="dropdown-item" href="#">Notification 2</a>
                                    <a class="dropdown-item" href="#">Notification 3</a>
                                    <a class="dropdown-item" href="#">Notification 4</a>
                                    <a class="dropdown-item" href="#">Another notification</a>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="nc-icon nc-zoom-split"></i>
                                    <span class="d-lg-block">&nbsp;Search</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="#pablo">
                                    <span class="no-icon">Account</span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="no-icon">Admin</span>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="/users/add">Add user</a>
                                    <a class="dropdown-item" href="/users">List users</a>
                                    <a class="dropdown-item" href="#">Something</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/logout">
                                    <span class="no-icon">Log out</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
			<!-- End Navbar -->
			<div class="content">
                <div class="container-fluid">
                    <div class="row">
						<?= $content_for_layout ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
