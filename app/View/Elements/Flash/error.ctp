<div class="alert alert-danger" role="alert">
  <strong>Oh no!</strong> <?= $message; ?>
</div>