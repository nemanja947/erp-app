
<div class="col-md-9 mx-auto">
    <?= $this->Form->create('User'); ?>
        <div class="form-group">
            <?= $this->Form->input('username', ['class' => 'form-control']); ?>
        </div>
        <div class="form-group">
            <?= $this->Form->input('password', ['class' => 'form-control']); ?>
        </div>
        <div class="form-group">
            <?=
                $this->Form->input('role', [
                    'options' => array('admin' => 'Admin', 'author' => 'Author'),
                    'class' => 'form-control'
                ]);
            ?>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
</div>
