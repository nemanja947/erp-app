<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $this->fetch('title'); ?>
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <?php
            echo $this->Html->css(['bootstrap.min', 'light-bootstrap-dashboard']);
            echo $this->Html->script(['core/jquery.3.2.1.min', 'core/popper.min', 'core/bootstrap.min', 'light-bootstrap-dashboard']);
        ?>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4 mx-auto">
                        <?php echo $this->Form->create('User'); ?>
                            <h2 class="form-signin-heading">Please sign in</h2>
                            <div class="form-group">
                                <?php echo $this->Form->input('username', ["class" => "form-control", "placeholder" => "Email address", 'label' => 'Username:']); ?>
                            </div>
                           
                            <div class="form-group">
                                <?php echo $this->Form->input('password', ["class" => "form-control", "placeholder" => "Password", 'label' => 'Password:']); ?>
                            </div>
                            <button class="btn btn-primary btn-block" type="submit">Sign in</button>
                        </form>
                    </div>
                </div>
            </div>
        </div> <!-- /container -->
    </body>
</html>
