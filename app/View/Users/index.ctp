<div class="col-md-12">
    <?= $this->Flash->render() ?>
    <div class="card strpied-tabled-with-hover">
        <div class="card-header ">
            <h4 class="card-title">Users</h4>
            <p class="card-category">This is list of users</p>
        </div>
        <div class="card-body table-full-width table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                    <th>Id</th>
                    <th>Username</th>
                    <th>Role</th>
                    <th>Created</th>
                    <th>Modified</th>
                </thead>
                <tbody>
                    <?php foreach ($users as $key => $user) { ?>
                        <tr>
                            <td><?= $user['User']['id'] ?></td>
                            <td><?= $user['User']['username'] ?></td>
                            <td><?= $user['User']['role'] ?></td>
                            <td><?= $user['User']['created'] ?></td>
                            <td><?= $user['User']['modified'] ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>